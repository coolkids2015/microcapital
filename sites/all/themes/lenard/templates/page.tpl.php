<?php
  $base_path = base_path();
  $path_to_theme = drupal_get_path('theme', 'lenard');
?>


<div id="opensearch" class="collapse myform">
		<div class="container">
            <?php print render($page['header_search']); ?>
		</div>
	</div>


<header class="header affix-top">
     	<div class="topbar">
        	<div class="container">
        <?php if ($page['social_menu']): ?>
<div class="social text-right">
            <?php print render($page['social_menu']); ?>
</div>
        <?php endif; ?>
        	</div><!-- end container -->
        </div><!-- end topbar -->

		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars"></i>
                    </button>
                    <a data-scroll class="navbar-brand" href="<?php print $front_page; ?>"><img src="<?php print $logo; ?>" alt=""></a>
				</div>
        <?php if ($page['main_menu']): ?>
          <div id="navbar-collapse" class="navbar-collapse collapse">
            <?php print render($page['main_menu']); ?>
          </div><!-- /.nav-collapse -->
        <?php endif; ?>
			</div><!--/.container-fluid -->
		</nav>

</header><!-- /.header -->
<section id="parallax5" class="page-header-section">

	</section>

<?php if ($page['hero']): ?>
	<article id="home" class="slider-wrapper">
				<?php print render($page['hero']); ?>
	</article>
<?php endif; ?>

<section class="section white-section">
        <div class="grey-shape"><div class="icon-container fixed border-radius"><i class="icon icon-Blog"></i></div></div>
        <div class="section-container">
            <div class="container">
                <div class="big-title">
                    <h3><span><?php print $title; ?></span></h3>
<?php print $breadcrumb; ?>
                    <div class="singlepage"></div>
                </div><!-- big-title -->
                



<div class="site-wrapper">
	<a id="main-content"></a>
	<?php if (($page['content']) || ($page['sidebar'])) : ?>
		<div class="main-content">
					<div class="row">
						<div class="<?php if ($page['sidebar']): ?> col-md-9 col-sm-9 col-xs-12 <?php else: ?> col-md-12 col-sm-12 col-xs-12 <?php endif; ?>">
							<?php if ($messages): ?>
								<div id="messages">
									<?php print $messages; ?>
								</div> <!-- /#messages -->
							<?php endif; ?>
							<?php if ($tabs_rendered = render($tabs)): ?>
								<div class="tabs">
									<?php print render($tabs); ?>
								</div>
							<?php endif; ?>
							<?php print render($page['content']); ?>
						</div>
						<?php if ($page['sidebar']): ?>
							<div class="col-md-3 col-sm-3 col-xs-12 sidebar blog-sidebar">
								<?php print render($page['sidebar']); ?>
							</div> <!-- /.sidebar -->
						<?php endif; ?>
					</div><!--/.row-->
			<?php print render($page['help']); ?>
			<?php if ($action_links): ?>
				<ul class="action-links">
					<?php print render($action_links); ?>
				</ul>
			<?php endif; ?>
			<?php if ($feed_icons): ?>
				<div class="container">
					<?php print $feed_icons; ?>
				</div>
			<?php endif; ?>
		</div><!-- /.main-content -->
	<?php endif; ?>
</div><!-- /.site-wrapper -->

            </div><!-- end container -->
        </div><!-- end section-container -->
    </section>





	<article id="contact" class="map-section">
		<div id="map" class="wow slideInUp<?php if (theme_get_setting('show_map') == 0): ?> hide<?php endif; ?>"></div>
	</article><!-- end section -->
    
    
	<section id="parallax4" class="section-parallax" data-stellar-background-ratio="0.1" data-stellar-vertical-offset="1">
    	<div class="parallax-wrapper">
        	<div class="container">
<?php if ($page['social_links']): ?>
				<?php print render($page['social_links']); ?>
<?php endif; ?>

                                
                <div class="row copyrights">
<?php if ($page['copyright']): ?>
				<?php print render($page['copyright']); ?>
<?php endif; ?>
                </div><!-- end row -->
        	</div><!-- end container -->
		</div><!-- end section-container -->
    </section><!-- end section -->
