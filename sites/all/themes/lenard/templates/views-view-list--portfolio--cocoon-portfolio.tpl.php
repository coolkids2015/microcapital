<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<?php print $wrapper_prefix; ?>
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <?php //print $list_type_prefix; ?>
  <ul class="portfolio-container clearfix image-hover-red hover-titles-effect masonry" id="masonry_grid">
    <?php foreach ($rows as $id => $row): ?>
      <li class="portfolio-item mix <?php print (str_replace(array(' ','ccnsep'),array('-', ' '),strtolower($classes_array[$id]))); ?>"><?php print $row; ?></li>
    <?php endforeach; ?>
  </ul>
  <?php //print $list_type_suffix; ?>
<?php print $wrapper_suffix; ?>