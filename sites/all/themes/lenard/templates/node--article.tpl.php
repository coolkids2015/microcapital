<?php
  global $base_path, $base_url;
?>

<div id="node-<?php print $node->nid; ?>" class="blog-container wow fadeIn clearfix animated <?php print $classes; ?>"<?php print $attributes; ?> role="article">
    <div class="post-media img-responsive-surround">
      <?php print render($content['field_image']); ?>
      <?php print render($content['field_image_slider']); ?>
      <?php print render($content['field_video']); ?>
      <div class="icon-container border-radius">
        <i class="icon <?php print render($content['field_post_type']); ?>"></i>
      </div>
    </div>
    <div class="post-top">
      <?php print render($title_prefix); ?>
      <h3 class="blog-post-title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h3>
      <?php print render($title_suffix); ?>
      <?php if ($display_submitted): ?>
        <ul class="list-inline">
          <li><strong>Posted by:</strong> <a href="<?php echo base_path() . 'user/' . $node->uid;  ?>"><?php print $node->name; ?></a></li>
          <li><strong>In:</strong> <?php print render($content['field_category']); ?></li>
          <li><strong>On:</strong> <?php print format_date($node->created, 'article'); ?></li>
        </ul>
      <?php endif; ?>
    </div>
    <div class="desc"<?php print $content_attributes; ?>>
      <?php
        hide($content['comments']);
        hide($content['links']);
      ?>
      <?php print render($content['body']); ?>
    </div>
    <div class="tags"><h3>TAGS <span><?php print render($content['field_tags']); ?></span></h3></div>
  <div class="clearfix"></div>
</div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>
