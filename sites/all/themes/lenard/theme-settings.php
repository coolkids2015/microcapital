<?php
function lenard_form_system_theme_settings_alter(&$form, &$form_state) {
  // Cocoon Options
  $form['cocoon_options'] = array(
      '#type' => 'vertical_tabs',
      '#title' => t('Cocoon Theme Options'),
      '#weight' => 0,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );
  // Cocoon Theme Skin Panel
  $form['cocoon_options']['cocoon_skin'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Cocoon Theme Skin'), 
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Cocoon Theme Skin Panel: Custom CSS
  $form['cocoon_options']['cocoon_skin']['custom_css'] = array(
    '#type' => 'textarea', 
    '#title' => t('Custom CSS'), 
    '#description' => t('Specify custom CSS tags and styling to apply to the theme. You can also override default styles here.'),
    '#rows' => '5',
    '#default_value' => theme_get_setting('custom_css'),
  );
  // Cocoon Theme Features Panel
  $form['cocoon_options']['cocoon_features'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Cocoon Theme Features'), 
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Cocoon Theme Features Panel: Preloader
  $form['cocoon_options']['cocoon_features']['preloader'] = array(
    '#type' => 'select',
    '#title' => t('Preloader'),
    '#description' => t('Show loading image while page loads?'),
    '#options' => array(
      0 => t('Disable'),
      1 => t('Only on homepage (recommended)'),
      2 => t('On all pages'),
    ),
    '#default_value' => theme_get_setting('preloader'),
  );
  // Cocoon Map Settings
  $form['cocoon_options']['cocoon_map'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Cocoon Map Settings'), 
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Cocoon Map Settings: Show Map on Homepage
  $form['cocoon_options']['cocoon_map']['show_map'] = array(
    '#type' => 'select',
    '#title' => t('Show Map'),
    '#description' => t('Display Google map on website homepage?'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => theme_get_setting('show_map'),
  );
  // Cocoon Map Settings: Latitude Textfield
  $form['cocoon_options']['cocoon_map']['map_latitude'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Latitude'),
    '#default_value' => theme_get_setting('map_latitude'),
    '#description'   => t("Enter the latitude of the address."),
  );
  // Cocoon Map Settings: Longitude Textfield
  $form['cocoon_options']['cocoon_map']['map_longitude'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Longitude'),
    '#default_value' => theme_get_setting('map_longitude'),
    '#description'   => t("Enter the longitude of the address."),
  );
  // Cocoon Map Settings: Map Marker
  $form['cocoon_options']['cocoon_map']['map_marker'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Map Marker'),
    '#default_value' => theme_get_setting('map_marker'),
    '#description'   => t("Path or absolute URL to the image to use as the Map Marker."),
  );
  // Cocoon Map Settings: Map Marker Title
  $form['cocoon_options']['cocoon_map']['map_marker_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Map Marker Title'),
    '#default_value' => theme_get_setting('map_marker_title'),
    '#description'   => t("Title displayed when Map Marker is clicked."),
  );
  // Cocoon Map Settings: Map Marker Title Link
  $form['cocoon_options']['cocoon_map']['map_marker_title_link'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Map Marker Title Link'),
    '#default_value' => theme_get_setting('map_marker_title_link'),
    '#description'   => t("Link when Map Marker Title is clicked, e.g. full map on Google Maps website."),
  );
  // Cocoon Map Settings: Map Marker Address
  $form['cocoon_options']['cocoon_map']['map_marker_address'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Map Marker Address'),
    '#default_value' => theme_get_setting('map_marker_address'),
    '#description'   => t("Address displayed when Map Marker is clicked."),
  );
  // Cocoon Map Settings: Map Marker Additional Information
  $form['cocoon_options']['cocoon_map']['map_marker_additional_information'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Map Marker Additional Information'),
    '#default_value' => theme_get_setting('map_marker_additional_information'),
    '#description'   => t("Additional text to display when Map Marker is clicked (e.g. phone number, directions, etc.)"),
  );
}
?>