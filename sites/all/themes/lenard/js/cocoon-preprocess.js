/* CREATED BY COCOON PREPROCESS.JS */
jQuery(document).ready(function($) {
  $('.header .topbar .social .menu a').empty();
  $('.open-search').attr('href', '#');
  $('.open-search').attr('data-toggle', 'collapse');
  $('.open-search').attr('data-target', '#opensearch');
  $('.open-search').removeAttr('data-scroll');
  $('.open-search').empty();
  $('#testimonials .testimonial:first-child').addClass('active');
  $('#testimonials .testimonial-nav li:first-child a').addClass('active');
  $('.features-widget .tabbable .nav-tabs li:nth-child(2)').addClass('active');  
  $('.features-widget .tabbable .tab-content .tab-pane:nth-child(2)').addClass('active');  
});